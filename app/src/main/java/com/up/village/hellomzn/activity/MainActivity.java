package com.up.village.hellomzn.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.up.village.hellomzn.R;
import com.up.village.hellomzn.Utils.UserData;
import com.up.village.hellomzn.adapter.DataAdapter;
import com.up.village.hellomzn.database.DBController;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private DBController dbController;

    private EditText edtSelectVillage;
    private TextView btnSelectPost;
    private String selectedPost;
    private Button btnSearch;

    private RecyclerView recyclerView;
    private LinearLayoutManager mLayoutManager;

    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        textViewTitle.setText("Hello Muzaffarnagar");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setToolbar();

        initView();

        dbController = new DBController(MainActivity.this);

        dbController.getAllUserData();

//        UserData userData = new UserData();
//        userData.setName("Demo");
//        userData.setMobile("111111111");
//        userData.setTehsilName("AAAA");
//        userData.setBlockName("HJJJ");
//        userData.setDesignation("NOTHING");
//        userData.setRevenueVillageName("DEMO");
//        dbController.insertDataToTrackTable(userData);
//
//        dbController.getAllUserData();
    }

    public void initView() {

        edtSelectVillage = findViewById(R.id.edt_village);
        btnSelectPost = findViewById(R.id.button_post);
        btnSearch = findViewById(R.id.btnEnter);

        recyclerView = findViewById(R.id.recyclerView);
        assert recyclerView != null;
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        btnSelectPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog postDialog;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    postDialog = new Dialog(MainActivity.this, android.R.style.Theme_Material_Light);
                } else {
                    postDialog = new Dialog(MainActivity.this, android.R.style.Theme_Light);
                }

                final String[] itemNames = getResources().getStringArray(R.array.my_array);

                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                        android.R.layout.simple_list_item_1, itemNames) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        View view = super.getView(position, convertView, parent);
                        TextView text = view.findViewById(android.R.id.text1);
                        text.setGravity(Gravity.CENTER);
                        text.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.app_bg_color_red));
                        return view;
                    }
                };

                postDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                postDialog.setContentView(R.layout.post_dialog);
                //postDialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
                postDialog.setCancelable(true);

                ListView lv = postDialog.findViewById(R.id.zone_list_view);
                lv.setAdapter(adapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String selectedZoneName = (String) parent.getItemAtPosition(position);
                        btnSelectPost.setText(selectedZoneName);
                        selectedPost = itemNames[position];
                        postDialog.dismiss();
                    }
                });

                if (!MainActivity.this.isFinishing()) {
                    postDialog.show();
                }
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtSelectVillage.getText().toString().trim().isEmpty()) {
                    //Toast.makeText(MainActivity.this, "Enter village name", Toast.LENGTH_SHORT).show();
                    showSnackBar("Enter village name");
                } else {
                    String post;

                    if (btnSelectPost.getText().toString().trim().equalsIgnoreCase("Select a Post")
                            || btnSelectPost.getText().toString().trim().equalsIgnoreCase("All Post")) {
                        post = "";
                    } else {
                        post = btnSelectPost.getText().toString().trim();
                    }
                    String villageName = edtSelectVillage.getText().toString().trim();

                    List<UserData> userDataList = new ArrayList<>();

                    userDataList.addAll(dbController.getSearchData(post, villageName));

                    if (userDataList.size() > 0) {
                        UserData userDataTitle = new UserData();

                        userDataTitle.setDesignation("Post");
                        userDataTitle.setRevenueVillageName("Village");
                        userDataTitle.setBlockName("Block");
                        userDataTitle.setName("Name");
                        userDataTitle.setMobile("Mobile");
                        userDataTitle.setTehsilName("Tehsil");

                        userDataList.add(0, userDataTitle);
                    }

                    DataAdapter dataAdapter = new DataAdapter(MainActivity.this, (ArrayList<UserData>) userDataList);
                    recyclerView.setAdapter(dataAdapter);
                }
            }
        });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    //make soft keyboard invisible when touch screen
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int[] scrcoords = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            //Log.d("Activity", "Touch event "+event.getRawX()+","+event.getRawY()+" "+x+","+y+" rect "+w.getLeft()+","+w.getTop()+","+w.getRight()+","+w.getBottom()+" coords "+scrcoords[0]+","+scrcoords[1]);
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    public void showSnackBar(String msg) {

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.coordinator), msg, Snackbar.LENGTH_SHORT);

        // Changing action button text color
//        View sbView = snackbar.getView();
//        TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
