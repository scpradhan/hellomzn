package com.up.village.hellomzn.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.up.village.hellomzn.R;
import com.up.village.hellomzn.Utils.UserData;
import com.up.village.hellomzn.activity.DetailsActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Salim on 5/3/2016.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<UserData> userDatas;

    public void add(int position, UserData item) {
        userDatas.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(UserData item) {
        int position = userDatas.indexOf(item);
        userDatas.remove(position);
        notifyItemRemoved(position);
    }

    public DataAdapter(Context context, ArrayList<UserData> item) {
        this.context = context;
        this.userDatas = item;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return userDatas.size();
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.village.setText(userDatas.get(position).getRevenueVillageName());
        holder.tehsil.setText(userDatas.get(position).getTehsilName());
        holder.block.setText(userDatas.get(position).getBlockName());
        holder.name.setText(userDatas.get(position).getName());
        holder.mobile.setText(userDatas.get(position).getMobile());
        holder.designation.setText(userDatas.get(position).getDesignation());

        if (position == 0) {

            holder.village.setTypeface(null, Typeface.BOLD);
            holder.tehsil.setTypeface(null, Typeface.BOLD);
            holder.block.setTypeface(null, Typeface.BOLD);
            holder.name.setTypeface(null, Typeface.BOLD);
            holder.mobile.setTypeface(null, Typeface.BOLD);
            holder.designation.setTypeface(null, Typeface.BOLD);
        } else {
            holder.village.setTypeface(null, Typeface.NORMAL);
            holder.tehsil.setTypeface(null, Typeface.NORMAL);
            holder.block.setTypeface(null, Typeface.NORMAL);
            holder.name.setTypeface(null, Typeface.NORMAL);
            holder.mobile.setTypeface(null, Typeface.NORMAL);
            holder.designation.setTypeface(null, Typeface.NORMAL);
        }

        holder.contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position != 0) {
                    Intent i = new Intent(context, DetailsActivity.class);
                    i.putExtra("data", userDatas.get(position));
                    context.startActivity(i);
                }
            }
        });

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView village;
        TextView tehsil;
        TextView block;
        TextView name;
        TextView mobile;
        TextView designation;

        View contentView;

        public ViewHolder(View v) {
            super(v);

            village = v.findViewById(R.id.village);
            tehsil = v.findViewById(R.id.tehsil);
            block = v.findViewById(R.id.block);
            name = v.findViewById(R.id.name);
            mobile = v.findViewById(R.id.mobile);
            designation = v.findViewById(R.id.designation);
            contentView = v;
        }
    }
}

