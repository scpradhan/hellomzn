package com.up.village.hellomzn.Utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Salim on 4/30/2016.
 */
public class UserData implements Parcelable {

    private long serialNo;
    private String designation;
    private String RevenueVillageName;
    private String TehsilName;
    private String BlockName;
    private String name;
    private String mobile;
//    private String PradhanName;
//    private String PradhanMobile;
//    private String VDOName;
//    private String VDOMobile;
//    private String ANMName;
//    private String ANMMobile;
//    private String AshaName;
//    private String AshaMobile;
//    private String AnganwadiWorker;
//    private String AnganwadiMobile;

    public UserData() {
        this.designation = "";
        this.RevenueVillageName = "";
        this.TehsilName = "";
        this.BlockName = "";
        this.name = "";
        this.mobile = "";
//        this.LekhpalName = "";
//        this.LekhpalMobile = "";
//        this.PradhanName = "";
//        this.PradhanMobile = "";
//        this.VDOName = "";
//        this.VDOMobile = "";
//        this.ANMName = "";
//        this.ANMMobile = "";
//        this.AshaName = "";
//        this.AshaMobile = "";
//        this.AnganwadiWorker = "";
//        this.AnganwadiMobile = "";
    }

    //Getter
    public long getSerialNo() {
        return serialNo;
    }

    public static Creator<UserData> getCREATOR() {
        return CREATOR;
    }

//    public String getAnganwadiMobile() {
//        return AnganwadiMobile;
//    }

//    public String getAnganwadiWorker() {
//        return AnganwadiWorker;
//    }
//
//    public String getANMMobile() {
//        return ANMMobile;
//    }
//
//    public String getANMName() {
//        return ANMName;
//    }
//
//    public String getAshaMobile() {
//        return AshaMobile;
//    }
//
//    public String getAshaName() {
//        return AshaName;
//    }

    public String getBlockName() {
        return BlockName;
    }

//    public String getLekhpalMobile() {
//        return LekhpalMobile;
//    }
//
//    public String getLekhpalName() {
//        return LekhpalName;
//    }
//
//    public String getPradhanMobile() {
//        return PradhanMobile;
//    }
//
//    public String getPradhanName() {
//        return PradhanName;
//    }

    public String getRevenueVillageName() {
        return RevenueVillageName;
    }

    public String getTehsilName() {
        return TehsilName;
    }

//    public String getVDOMobile() {
//        return VDOMobile;
//    }
//
//    public String getVDOName() {
//        return VDOName;
//    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getMobile() {
        return mobile;
    }
    //Setter


//    public void setAnganwadiMobile(String anganwadiMobile) {
//        AnganwadiMobile = anganwadiMobile;
//    }
//
//    public void setAnganwadiWorker(String anganwadiWorker) {
//        AnganwadiWorker = anganwadiWorker;
//    }
//
//    public void setANMMobile(String ANMMobile) {
//        this.ANMMobile = ANMMobile;
//    }
//
//    public void setANMName(String ANMName) {
//        this.ANMName = ANMName;
//    }
//
//    public void setAshaMobile(String ashaMobile) {
//        AshaMobile = ashaMobile;
//    }
//
//    public void setAshaName(String ashaName) {
//        AshaName = ashaName;
//    }

    public void setBlockName(String blockName) {
        BlockName = blockName;
    }

//    public void setLekhpalMobile(String lekhpalMobile) {
//        LekhpalMobile = lekhpalMobile;
//    }
//
//    public void setLekhpalName(String lekhpalName) {
//        LekhpalName = lekhpalName;
//    }
//
//    public void setPradhanMobile(String pradhanMobile) {
//        PradhanMobile = pradhanMobile;
//    }
//
//    public void setPradhanName(String pradhanName) {
//        PradhanName = pradhanName;
//    }

    public void setRevenueVillageName(String revenueVillageName) {
        RevenueVillageName = revenueVillageName;
    }

    public void setSerialNo(long serialNo) {
        this.serialNo = serialNo;
    }

    public void setTehsilName(String tehsilName) {
        TehsilName = tehsilName;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setName(String name) {
        this.name = name;
    }
    //
//    public void setVDOMobile(String VDOMobile) {
//        this.VDOMobile = VDOMobile;
//    }
//
//    public void setVDOName(String VDOName) {
//        this.VDOName = VDOName;
//    }

    protected UserData(Parcel in) {
        serialNo = in.readLong();
        designation = in.readString();
        RevenueVillageName = in.readString();
        TehsilName = in.readString();
        BlockName = in.readString();
        name = in.readString();
        mobile = in.readString();
//        LekhpalName = in.readString();
//        LekhpalMobile = in.readString();
//        LekhpalName = in.readString();
//        LekhpalMobile = in.readString();
//        PradhanName = in.readString();
//        PradhanMobile = in.readString();
//        VDOName = in.readString();
//        VDOMobile = in.readString();
//        ANMName = in.readString();
//        ANMMobile = in.readString();
//        AshaName = in.readString();
//        AshaMobile = in.readString();
//        AnganwadiWorker = in.readString();
//        AnganwadiMobile = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(serialNo);
        dest.writeString(designation);
        dest.writeString(RevenueVillageName);
        dest.writeString(TehsilName);
        dest.writeString(BlockName);
        dest.writeString(name);
        dest.writeString(mobile);
//        dest.writeString(LekhpalName);
//        dest.writeString(LekhpalMobile);
//        dest.writeString(PradhanName);
//        dest.writeString(PradhanMobile);
//        dest.writeString(VDOName);
//        dest.writeString(VDOMobile);
//        dest.writeString(ANMName);
//        dest.writeString(ANMMobile);
//        dest.writeString(AshaName);
//        dest.writeString(AshaMobile);
//        dest.writeString(AnganwadiWorker);
//        dest.writeString(AnganwadiMobile);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserData> CREATOR = new Parcelable.Creator<UserData>() {
        @Override
        public UserData createFromParcel(Parcel in) {
            return new UserData(in);
        }

        @Override
        public UserData[] newArray(int size) {
            return new UserData[size];
        }
    };
}
