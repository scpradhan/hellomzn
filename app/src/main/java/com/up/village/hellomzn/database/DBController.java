package com.up.village.hellomzn.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.up.village.hellomzn.Utils.UserData;

import java.util.ArrayList;

/**
 * Created by Salim on 4/30/2016.
 */
public class DBController extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "user.db";
    private static final int DATABASE_VERSION = 1;

    private final String TABLE_NAME = "User";

    public DBController(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<UserData> getAllUserData() {

        ArrayList<UserData> data_list = new ArrayList<>();

        try {
            // open database to query
            SQLiteDatabase mySqliteDb = getWritableDatabase();

            String selectQuery = "SELECT * FROM " + TABLE_NAME;

            Cursor cursor = mySqliteDb.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    UserData data = new UserData();

                    data.setSerialNo(cursor.getInt(0));

                    if (cursor.getString(1) != null) {
                        data.setDesignation(cursor.getString(1));
                    }
                    if (cursor.getString(2) != null) {
                        data.setRevenueVillageName(cursor.getString(2));
                    }
                    if (cursor.getString(3) != null) {
                        data.setTehsilName(cursor.getString(3));
                    }
                    if (cursor.getString(4) != null) {
                        data.setBlockName(cursor.getString(4));
                    }
                    if (cursor.getString(5) != null) {
                        data.setName(cursor.getString(5));
                    }
                    if (cursor.getString(6) != null) {
                        data.setMobile(cursor.getString(6));
                    }
//                    if (cursor.getString(4) != null) {
//                        data.setLekhpalName(cursor.getString(4));
//                    }
//                    if (cursor.getString(5) != null) {
//                        data.setLekhpalMobile(cursor.getString(5));
//                    }
//                    if (cursor.getString(6) != null) {
//                        data.setPradhanName(cursor.getString(6));
//                    }
//                    if (cursor.getString(7) != null) {
//                        data.setPradhanMobile(cursor.getString(7));
//                    }
//                    if (cursor.getString(8) != null) {
//                        data.setVDOName(cursor.getString(8));
//                    }
//                    if (cursor.getString(9) != null) {
//                        data.setVDOMobile(cursor.getString(9));
//                    }
//                    if (cursor.getString(10) != null) {
//                        data.setANMName(cursor.getString(10));
//                    }
//                    if (cursor.getString(11) != null) {
//                        data.setANMMobile(cursor.getString(11));
//                    }
//                    if (cursor.getString(12) != null) {
//                        data.setAshaName(cursor.getString(12));
//                    }
//                    if (cursor.getString(13) != null) {
//                        data.setAshaMobile(cursor.getString(13));
//                    }
//                    if (cursor.getString(14) != null) {
//                        data.setAnganwadiWorker(cursor.getString(14));
//                    }
//                    if (cursor.getString(15) != null) {
//                        data.setAnganwadiMobile(cursor.getString(15));
//                    }

                    data_list.add(data);
                } while (cursor.moveToNext());
                Log.e("all)data", "" + data_list.size());
            }

            cursor.close();

        } catch (Exception e) {

            Log.e("exception", "" + e);
        }

        close();
        return data_list;
    }

    // Insert
    public void insertDataToTrackTable(UserData queryValues) {

//        CREATE TABLE [User] (SI integer NOT NULL PRIMARY KEY AUTOINCREMENT,
//        Designation text,RevenueVillageName text,TehsilName text,BlockName text,Name text,Mobile text)
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("Designation", queryValues.getDesignation());
        values.put("RevenueVillageName", queryValues.getRevenueVillageName());
        values.put("TehsilName", queryValues.getTehsilName());
        values.put("BlockName", queryValues.getBlockName());
        values.put("Name", queryValues.getName());
        values.put("Mobile", queryValues.getMobile());

        database.insert(TABLE_NAME, null, values);
        database.close();
    }

    public ArrayList<UserData> getSearchData(String post, String villageName) {

        ArrayList<UserData> data_list = new ArrayList<>();

        try {
            // open database to query
            SQLiteDatabase mySqliteDb = getWritableDatabase();

//            CREATE TABLE [User] (SI integer NOT NULL PRIMARY KEY AUTOINCREMENT,
//            designation text,RevenueVillageName text,TehsilName text,BlockName text,
//                    name text,mobile text)

            String selectQuery = "";

            if (!post.isEmpty()) {
                selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE RevenueVillageName LIKE '" + villageName + "%' AND Designation LIKE '%" + post + "%'";
            } else {
                selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE RevenueVillageName LIKE '" + villageName + "%'";
            }

            Cursor cursor = mySqliteDb.rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {

                do {
                    UserData data = new UserData();

                    data.setSerialNo(cursor.getInt(0));

                    if (cursor.getString(1) != null) {
                        data.setDesignation(cursor.getString(1));
                    }
                    if (cursor.getString(2) != null) {
                        data.setRevenueVillageName(cursor.getString(2));
                    }
                    if (cursor.getString(3) != null) {
                        data.setTehsilName(cursor.getString(3));
                    }
                    if (cursor.getString(4) != null) {
                        data.setBlockName(cursor.getString(4));
                    }
                    if (cursor.getString(5) != null) {
                        data.setName(cursor.getString(5));
                    }
                    if (cursor.getString(6) != null) {
                        data.setMobile(cursor.getString(6));
                    }
//                    if (cursor.getString(4) != null) {
//                        data.setLekhpalName(cursor.getString(4));
//                    }
//                    if (cursor.getString(5) != null) {
//                        data.setLekhpalMobile(cursor.getString(5));
//                    }
//                    if (cursor.getString(6) != null) {
//                        data.setPradhanName(cursor.getString(6));
//                    }
//                    if (cursor.getString(7) != null) {
//                        data.setPradhanMobile(cursor.getString(7));
//                    }
//                    if (cursor.getString(8) != null) {
//                        data.setVDOName(cursor.getString(8));
//                    }
//                    if (cursor.getString(9) != null) {
//                        data.setVDOMobile(cursor.getString(9));
//                    }
//                    if (cursor.getString(10) != null) {
//                        data.setANMName(cursor.getString(10));
//                    }
//                    if (cursor.getString(11) != null) {
//                        data.setANMMobile(cursor.getString(11));
//                    }
//                    if (cursor.getString(12) != null) {
//                        data.setAshaName(cursor.getString(12));
//                    }
//                    if (cursor.getString(13) != null) {
//                        data.setAshaMobile(cursor.getString(13));
//                    }
//                    if (cursor.getString(14) != null) {
//                        data.setAnganwadiWorker(cursor.getString(14));
//                    }
//                    if (cursor.getString(15) != null) {
//                        data.setAnganwadiMobile(cursor.getString(15));
//                    }

                    data_list.add(data);
                } while (cursor.moveToNext());
                Log.e("all data", "" + data_list.size());
            }

            cursor.close();

        } catch (Exception e) {

            Log.e("exception", "" + e);
        }

        close();
        return data_list;
    }
}
