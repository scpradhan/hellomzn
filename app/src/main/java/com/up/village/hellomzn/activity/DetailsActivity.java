package com.up.village.hellomzn.activity;

import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.up.village.hellomzn.R;
import com.up.village.hellomzn.Utils.UserData;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

/**
 * Created by Salim on 5/3/2016.
 */
public class DetailsActivity extends AppCompatActivity {

    TextView village;
    TextView tehsil;
    TextView block;
    TextView name;
    TextView mobile;
    TextView designation;


    private void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView textViewTitle = toolbar.findViewById(R.id.toolbar_title);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        textViewTitle.setText("Details");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        //setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setTitle("Hello Muzaffarnagar");

        setToolbar();

        UserData userData = getIntent().getExtras().getParcelable("data");

        village = findViewById(R.id.dt_village);
        tehsil = findViewById(R.id.dt_tehsil);
        block = findViewById(R.id.dt_block);
        name = findViewById(R.id.dt_name);
        mobile = findViewById(R.id.dt_mobile);
        designation = findViewById(R.id.dt_designation);

        if (userData != null) {
            village.setText(Html.fromHtml("<b>Village: </b>" + userData.getRevenueVillageName()));
            tehsil.setText(Html.fromHtml("<b>Tehsil: </b>" + userData.getTehsilName()));
            block.setText(Html.fromHtml("<b>Block: </b>" + userData.getBlockName()));
            name.setText(Html.fromHtml("<b>Name: </b>" + userData.getName()));
            mobile.setText(Html.fromHtml("<b>Mobile: </b>" + userData.getMobile()));
            designation.setText(Html.fromHtml("<b>Designation: </b>" + userData.getDesignation()));
        } else {
            Toast.makeText(DetailsActivity.this, "NULL", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
